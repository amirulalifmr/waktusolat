import requests
import urllib
from collections import OrderedDict
import json
from bs4 import BeautifulSoup

def connect():
    global urlink
    urlink = "https://www.islamicfinder.org/world/malaysia/44595529/nilai-prayer-times/?language=ms"
    headers = {"User-Agent":"Mozilla/5.0 (X11: Ubuntu; Linux x86_64; rv:16.0; Rover) Gecko/20100101 Firefox/16.0"}
    r = requests.get(urlink,headers=headers)
    return r

def craw(data):
    global month; global sub; global syu; global zoh; global asr; global mgh; global ish
    global day; global subb; global syuu; global zohh; global asrr; global mghh; global ishh

    soup = BeautifulSoup(data, 'html.parser')
    table = soup.find('table')
    title = table.findAll(class_="row-title")
    for x in title:
        month = x.contents[1].text.strip()
        sub = x.contents[7].text.strip()
        syu = x.contents[9].text.strip()
        zoh = x.contents[11].text.strip()
        asr = x.contents[13].text.strip()
        mgh = x.contents[15].text.strip()
        ish = x.contents[17].text.strip()

    today = table.findAll(class_="row-body row-today")
    for x in today:
        day = x.contents[1].text.strip()
        subb = x.contents[7].text.strip()
        syuu = x.contents[9].text.strip()
        zohh = x.contents[11].text.strip()
        asrr = x.contents[13].text.strip()
        mghh = x.contents[15].text.strip()
        ishh = x.contents[17].text.strip()

    return

def main():

    page = connect()
    data = page.content
    craw(data)
    status = page.status_code
    fulldate = month + " " + day

    #Create json file
    data = {"Status": status, "Bulan": month, "Tarikh": fulldate, sub: subb, syu: syuu, zoh: zohh, asr: asrr, mgh: mghh,ish: ishh}

    with open("waktusolatnilai.json","w") as write_file:
        json.dumps(data, write_file)

    json_string = json.dumps(data, indent=2)
    print json_string

if __name__=="__main__":
    main()